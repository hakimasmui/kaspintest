package com.example.kaspinfinaltes.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import com.example.kaspinfinaltes.MainActivity;
import com.example.kaspinfinaltes.model.BarangModel;

public class DatabaseHelper extends SQLiteOpenHelper {

    public DatabaseHelper(Context context) {
        super(context, "kaspin.db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS tb_barang(id integer primary key autoincrement, kode_barang TEXT, nama_barang TEXT, harga_jual REAL, harga_beli REAL)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public void simpanBarang(BarangModel barangModel) {
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();

        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put("kode_barang", barangModel.getKode_barang());
            contentValues.put("nama_barang", barangModel.getNama_barang());
            contentValues.put("harga_jual", barangModel.getHarga_jual());
            contentValues.put("harga_beli", barangModel.getHarga_beli());

            db.insert("tb_barang", null, contentValues);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
        }
    }
}
