package com.example.kaspinfinaltes.repository;

import android.app.Application;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.kaspinfinaltes.db.DatabaseHelper;
import com.example.kaspinfinaltes.model.BarangModel;

public class BarangRepository {

    DatabaseHelper dbHelper;

    public BarangRepository(Application app) {
        dbHelper = new DatabaseHelper(app);
    }

    public void simpanBarang(BarangModel barangModel) {
        dbHelper.simpanBarang(barangModel);
    }

    public void fetchBarang() {

    }
}