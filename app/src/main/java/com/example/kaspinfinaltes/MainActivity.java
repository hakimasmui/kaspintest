package com.example.kaspinfinaltes;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import com.example.kaspinfinaltes.db.DatabaseHelper;
import com.example.kaspinfinaltes.model.BarangModel;
import com.example.kaspinfinaltes.viewModel.BarangViewModel;

public class MainActivity extends AppCompatActivity {

    EditText edtKode, edtNama, edtHargaJual, edtHargaBeli;
    Button btnSimpan;

    BarangViewModel barangViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        barangViewModel = new ViewModelProvider(this).get(BarangViewModel.class);

        edtKode = findViewById(R.id.edtKode);
        edtNama = findViewById(R.id.edtNama);
        edtHargaJual = findViewById(R.id.edtHargaJual);
        edtHargaBeli = findViewById(R.id.edtHargaBeli);
        btnSimpan = findViewById(R.id.btnSimpan);

        btnSimpan.setOnClickListener(v -> {
            String kode_barang = edtKode.getText().toString();
            String nama_barang = edtNama.getText().toString();
            double harga_beli = Double.parseDouble(edtHargaBeli.getText().toString());
            double harga_jual = Double.parseDouble(edtHargaJual.getText().toString());

            BarangModel barangModel = new BarangModel(kode_barang, nama_barang, harga_jual, harga_beli);
            barangViewModel.simpanBarang(barangModel);
        });
    }
}