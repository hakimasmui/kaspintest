package com.example.kaspinfinaltes.model;

public class BarangModel {
    String kode_barang;
    String nama_barang;
    double harga_jual;
    double harga_beli;

    public BarangModel(String kode_barang, String nama_barang, double harga_jual, double harga_beli) {
        this.kode_barang = kode_barang;
        this.nama_barang = nama_barang;
        this.harga_jual = harga_jual;
        this.harga_beli = harga_beli;
    }

    public String getKode_barang() {
        return kode_barang;
    }

    public void setKode_barang(String kode_barang) {
        this.kode_barang = kode_barang;
    }

    public String getNama_barang() {
        return nama_barang;
    }

    public void setNama_barang(String nama_barang) {
        this.nama_barang = nama_barang;
    }

    public double getHarga_jual() {
        return harga_jual;
    }

    public void setHarga_jual(double harga_jual) {
        this.harga_jual = harga_jual;
    }

    public double getHarga_beli() {
        return harga_beli;
    }

    public void setHarga_beli(double harga_beli) {
        this.harga_beli = harga_beli;
    }
}
